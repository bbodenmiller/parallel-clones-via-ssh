1. Add testfile.pub to Project Settings > Repository > Deploy Keys
1. Adjust .gitlab-ci.yml to drive as much parallel git-over-ssh traffic as you'd like.
1. The clone URL should automatically self-clone the project the pipeline runs within
