#!/bin/bash
GIT_SSH_COMMAND="ssh -i /tmp/testfile -o StrictHostKeyChecking=no"
for i in {1..$CLONES_PER_JOB}
    do
        git clone git@$CI_SERVER_HOST:$CI_PROJECT_PATH repo-$i
        touch repo-$i/$i
        ls -la repo-$i
        rm -rf repo
        echo "clone $i"
    done
